// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class JewelECS : ModuleRules
{
	public JewelECS(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bUseRTTI = true;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "JewelECSCore",
            "BichuECS",
            "BichuCommon",
            "BichuExtension"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { "UMG" });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
