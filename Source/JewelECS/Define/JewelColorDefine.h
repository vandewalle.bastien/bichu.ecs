#pragma once

#include "ObjectMacros.h"
#include "ECS/Define/JewelDefine.h"

UENUM(BlueprintType)
enum class UEJewelColor : uint8
{
	JC_Red		= (uint8)EJewelColor::JC_Red		UMETA(DisplayName = "Red"),
	JC_Blue		= (uint8)EJewelColor::JC_Blue		UMETA(DisplayName = "Blue"),
	JC_Green	= (uint8)EJewelColor::JC_Green		UMETA(DisplayName = "Green"),
	JC_Yellow	= (uint8)EJewelColor::JC_Yellow		UMETA(DisplayName = "Yellow"),
	JC_None		= (uint8)EJewelColor::JC_None		UMETA(DisplayName = "None"),
};
