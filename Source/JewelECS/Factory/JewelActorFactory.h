#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "ECS/Factory/JewelFactory.h"
#include "Templates/SubclassOf.h"
#include "JewelActorFactory.generated.h"

class IEntityObjectFactory;
class FComponentFactory;

class AActor;
class UJewelScoreWidget;

UCLASS(Blueprintable, Abstract)
class UJewelActorFactory : public UObject, public IJewelFactory
{
	GENERATED_BODY()

public:
	void Setup(IEntityObjectFactory * inEntityFactory, const TWeakPtr<FComponentFactory> & inComponentFactory) override final;

public:
	uint32 BuildColorJewel(uint32 inGridId, EJewelColor inColor, int32 inPosX, int32 inPosY, float inSpacing) const override final;
	void BuildJewelGrid() const override final;
	void BuildJewelGameMode() const override final;
	void BuildJewelScore() const override final;

	void DestroyJewel(uint32 EntityId) const override final;

protected:
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActor> JewelActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActor> JewelGridClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UJewelScoreWidget> JewelScoreClass;

private:
	IEntityObjectFactory * entityFactory;
	TWeakPtr<FComponentFactory> componentFactory;

private:
	void AddJewelToGrid(AActor * Jewel, uint32 GridEntityId, int32 Row, int32 Column) const;
};
