#include "JewelActorFactory.h"

// Engine
#include "Engine/World.h"

// ECS
#include "Bichu/ECS/ComponentFactory.h"
#include "Bichu/Extension/IEntityObjectFactory.h"

// Component
#include "ECS/Component/JewelGameStateComponent.h"
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/ColorJewelComponent.h"
#include "ECS/Component/JewelScoreInfoComponent.h"
#include "ECS/Component/JewelScoreVisualComponent.h"
#include "ECS/Component/JewelMovementComponent.h"
#include "ECS/Component/JewelTransformComponent.h"
#include "ECS/Component/SelectableComponent.h"

// Widget
#include "Implementator/Widget/JewelScoreWidget.h"

// Scene Component
#include "Implementator/Component/JewelGridSceneComponent.h"

// Object
#include "Implementator/EmptyObject.h"

void UJewelActorFactory::Setup(IEntityObjectFactory * inEntityFactory, const TWeakPtr<FComponentFactory> & inComponentFactory)
{
	entityFactory = inEntityFactory;
	componentFactory = inComponentFactory;
}

uint32 UJewelActorFactory::BuildColorJewel(uint32 inGridId, EJewelColor inColor, int32 inPosX, int32 inPosY, float inSpacing) const
{
	AActor * jewelActor = GetWorld()->SpawnActor(JewelActorClass);
	AddJewelToGrid(jewelActor, inGridId, inPosY, inPosX);

	FJewelInfoComponent * jewelInfoComp = componentFactory.Pin()->CreateComponent<FJewelInfoComponent>();

	FEntityInitializer initializer = entityFactory->BuildEntityObject(jewelActor, { jewelInfoComp });

	initializer.Init(FJewelInfoComponent(inGridId, inPosX, inPosY));

	ISelectableComponent * selectableComp = initializer.GetComponent<ISelectableComponent>();
	check(selectableComp != nullptr);
	selectableComp->Selection = TDispatchOnSet<bool>(initializer.GetEntityId(), false);

	IColorJewelComponent * colorJewelComp = initializer.GetComponent<IColorJewelComponent>();
	check(colorJewelComp != nullptr);
	colorJewelComp->SetColor(inColor);

	IJewelTransformComponent * jewelTransformComp = initializer.GetComponent<IJewelTransformComponent>();
	check(jewelTransformComp != nullptr);
	jewelTransformComp->SetPosition(FVector(inPosX * inSpacing, inPosY * inSpacing, 0.0f));

	return initializer.GetEntityId();
}

void UJewelActorFactory::AddJewelToGrid(AActor * Jewel, uint32 GridEntityId, int32 Row, int32 Column) const
{
	AActor * jewelGridActor = entityFactory->GetEntityObject<AActor>(GridEntityId);

	UActorComponent * jewelGridActorComponent = jewelGridActor->GetComponentByClass(UJewelGridSceneComponent::StaticClass());
	UJewelGridSceneComponent * jewelGridComp = CastChecked<UJewelGridSceneComponent>(jewelGridActorComponent);

	jewelGridComp->AddJewel(Jewel, Row, Column);
}

void UJewelActorFactory::BuildJewelGrid() const
{
	AActor * jewelGridActor = GetWorld()->SpawnActor(JewelGridClass);

	entityFactory->BuildEntityObject(jewelGridActor);
}

void UJewelActorFactory::BuildJewelGameMode() const
{
	auto gameModeObject = NewObject<UEmptyObject>();

	auto gameStateComp = componentFactory.Pin()->CreateComponent<FJewelGameStateComponent>();

	FEntityInitializer entityInitializer = entityFactory->BuildEntityObject(gameModeObject, { gameStateComp });

	const EJewelGameStep gameStep = EJewelGameStep::JGS_None;
	gameStateComp->gameStep = TDispatchOnSet<EJewelGameStep>(entityInitializer.GetEntityId(), gameStep);
}

void UJewelActorFactory::BuildJewelScore() const
{
	UJewelScoreWidget * jewelScoreWidget = CreateWidget<UJewelScoreWidget>(GetWorld(), JewelScoreClass);
	jewelScoreWidget->AddToViewport();

	auto jewelScoreInfoComp = componentFactory.Pin()->CreateComponent<FJewelScoreInfoComponent>();

	IJewelScoreVisualComponent * jewelScoreVisualComp = static_cast<IJewelScoreVisualComponent *>(jewelScoreWidget);

	TSet<IComponent *> allComponents = { jewelScoreVisualComp, jewelScoreInfoComp };

	entityFactory->BuildEntityObject(jewelScoreWidget, allComponents);
}

void UJewelActorFactory::DestroyJewel(uint32 EntityId) const
{
	UObject * jewelObject = entityFactory->DestroyEntityObject(EntityId);

	if (jewelObject != nullptr)
	{
		AActor * jewelActor = Cast<AActor>(jewelObject);
		check(jewelActor);
		jewelActor->Destroy();
	}
}
