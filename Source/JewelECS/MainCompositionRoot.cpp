#include "MainCompositionRoot.h"

// Engine
#include "EngineUtils.h"

// Engine
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

// ECS
#include "Bichu/ECS/SystemRootFactory.h"
#include "Bichu/ECS/SystemRoot.h"
#include "Bichu/Extension/Scheduler/UnrealScheduler.h"

// Input
#include "Input/PlayerInputManager.h"

// Factory
#include "Bichu/Extension/Factory/EntityObjectFactoryImpl.h"
#include "Factory/JewelActorFactory.h"

// Systems
#include "ECS/System/JewelSpawnSystem.h"
#include "ECS/System/JewelSwapSystem.h"
#include "ECS/System/JewelDestroySystem.h"
#include "ECS/System/JewelGameModeSystem.h"
#include "ECS/System/JewelScoreSystem.h"
#include "ECS/System/JewelMoveSystem.h"
#include "ECS/System/SelectionSystem.h"

void AMainCompositionRoot::BeginPlay()
{
	Super::BeginPlay();

	SetupSystems();

	BuildEntitiesFromScene();
}

void AMainCompositionRoot::SetupSystems()
{
	scheduler = GetWorld()->SpawnActor<AUnrealScheduler>();
	systemRoot = FSystemRootFactory::Build(scheduler);
	check(systemRoot.IsValid());

	// Factory
	TSharedPtr<IEntityFactory> entityFactory = systemRoot->CreateEntityFactory();
	componentFactory = systemRoot->CreateComponentFactory();

	entityObjectFactory = NewObject<UEntityObjectFactoryImpl>();
	entityObjectFactory->Setup(entityFactory);

	jewelFactory = NewObject<UJewelActorFactory>(this, JewelFactoryClass);
	jewelFactory->Setup(entityObjectFactory, componentFactory);

	// Input
	APlayerController * playerController = GetWorld()->GetFirstPlayerController();
	UInputComponent * inputComponent = playerController->InputComponent;

	playerInput = NewObject<UPlayerInputManager>();
	playerInput->BindInput(inputComponent);
	 
	// Create Systems
	FSelectionSystem * selectionSystem = new FSelectionSystem();
	playerInput->SubscribeMouseObserver(selectionSystem);

	// Add Systems
	systemRoot->AddSystem(selectionSystem);
	systemRoot->AddSystem(new FJewelGameModeSystem(jewelFactory));
	systemRoot->AddSystem(new FJewelDestroySystem(jewelFactory));
	systemRoot->AddSystem(new FJewelSpawnSystem(jewelFactory));
	systemRoot->AddSystem(new FJewelSwapSystem());
	systemRoot->AddSystem(new FJewelScoreSystem(jewelFactory));
	systemRoot->AddSystem(new FJewelMoveSystem());
}

void AMainCompositionRoot::BuildEntitiesFromScene()
{
	for (TActorIterator<AActor> Itr(GetWorld()); Itr; ++Itr)
	{
		entityObjectFactory->BuildEntityObject(*Itr);
	}
}

void AMainCompositionRoot::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}
