#include "PlayerInputManager.h"

#include "Input/Event/MouseInputDispatcher.h"

#include "Components/InputComponent.h"

UPlayerInputManager::UPlayerInputManager()
	: Super()
	, inputDispatcher(new FMouseInputDispatcher())
{
}

void UPlayerInputManager::BindInput(UInputComponent * inputComponent)
{
	inputComponent->BindAction("LeftMouseDown", IE_Pressed, this, &UPlayerInputManager::OnLeftClick);
}

void UPlayerInputManager::SubscribeMouseObserver(IMouseInputObserver * mouseObserver)
{
	check(mouseObserver != nullptr);
	inputDispatcher->SubscribeObserver(*mouseObserver);
}

void UPlayerInputManager::OnLeftClick()
{
	inputDispatcher->BroadcastLeftClick();
}