#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "PlayerInputManager.generated.h"

class UInputComponent;

class FMouseInputDispatcher;
class IMouseInputObserver;

UCLASS()
class UPlayerInputManager : public UObject
{
	GENERATED_BODY()

	UPlayerInputManager();

public:
	void BindInput(UInputComponent * inputComponent);

public:
	void SubscribeMouseObserver(IMouseInputObserver * mouseObserver);

private:
	TSharedPtr<FMouseInputDispatcher> inputDispatcher;

private:
	void OnLeftClick();
};
