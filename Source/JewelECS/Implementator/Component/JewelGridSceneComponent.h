#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "ECS/Component/JewelGridComponent.h"
#include "Bichu/Extension/Implementator.h"
#include "JewelGridSceneComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API UJewelGridSceneComponent : public USceneComponent, public IJewelGridComponent, public IImplementator
{
	GENERATED_BODY()

public: // IJewelGridComponent
	UFUNCTION(BlueprintImplementableEvent)
	int32 GetWidth() const override;

	UFUNCTION(BlueprintImplementableEvent)
	int32 GetHeight() const override;

	UFUNCTION(BlueprintImplementableEvent)
	float GetSpacing() const override;

public:
	UFUNCTION(BlueprintImplementableEvent)
	void AddJewel(AActor * Jewel, int32 Row, int32 Column);
};
