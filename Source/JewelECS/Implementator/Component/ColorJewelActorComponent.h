#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ECS/Component/ColorJewelComponent.h"
#include "Bichu/Extension/Implementator.h"
#include "Define/JewelColorDefine.h"
#include "ColorJewelActorComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API UColorJewelActorComponent : public UActorComponent, public IColorJewelComponent, public IImplementator
{
	GENERATED_BODY()

public: // IColorJewelComponent
	void SetColor(const EJewelColor Color) override		{ BPSetColor(static_cast<UEJewelColor>(Color)); }
	EJewelColor GetColor() const override				{ return static_cast<EJewelColor>(BPGetColor()); }

	UFUNCTION(BlueprintImplementableEvent)
	void SetHighlight(bool isHighlighted) override;

public:
	UFUNCTION(BlueprintImplementableEvent, DisplayName = "Set Color")
	void BPSetColor(const UEJewelColor Color);

	UFUNCTION(BlueprintImplementableEvent, DisplayName = "Get Color")
	UEJewelColor BPGetColor() const;
};
