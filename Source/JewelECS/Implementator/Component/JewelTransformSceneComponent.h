#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "ECS/Component/JewelTransformComponent.h"
#include "Bichu/Extension/Implementator.h"
#include "JewelTransformSceneComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API UJewelTransformSceneComponent :
	public USceneComponent,
	public IJewelTransformComponent,
	public IImplementator
{
	GENERATED_BODY()

public: // IJewelMovementComponent
	UFUNCTION(BlueprintImplementableEvent)
	void SetPosition(const FVector & Position) override;
};
