#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ECS/Component/JewelMovementComponent.h"
#include "Bichu/Extension/Implementator.h"
#include "JewelMovementActorComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API UJewelMovementActorComponent :
	public UActorComponent,
	public IJewelMovementComponent,
	public IImplementator
{
	GENERATED_BODY()

public: // IJewelMovementComponent
	UFUNCTION(BlueprintImplementableEvent)
	float GetFallSpeed() const override;

	UFUNCTION(BlueprintImplementableEvent)
	float GetSwapSpeed() const override;
};
