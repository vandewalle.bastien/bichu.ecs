#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ECS/Component/SelectableComponent.h"
#include "Bichu/Extension/Implementator.h"
#include "SelectableActorComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API USelectableActorComponent : public UActorComponent, public ISelectableComponent, public IImplementator
{
	GENERATED_BODY()

public: // ISelectableComponent
	UFUNCTION(BlueprintImplementableEvent)
	bool IsOverlapped() const override;
};
