#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ECS/Component/JewelEffectComponent.h"
#include "Bichu/Extension/Implementator.h"
#include "JewelEffectActorComponent.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API UJewelEffectActorComponent :
	public UActorComponent,
	public IJewelEffectComponent,
	public IImplementator
{
	GENERATED_BODY()

public: // IJewelEffectComponent
	UFUNCTION(BlueprintImplementableEvent)
	void PlayDestruction() const override;
};
