#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "EmptyObject.generated.h"

UCLASS()
class UEmptyObject : public UObject
{
	GENERATED_BODY()
};
