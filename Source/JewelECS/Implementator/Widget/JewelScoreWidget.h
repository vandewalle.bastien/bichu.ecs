#pragma once

#include "CoreMinimal.h"
#include "UserWidget.h"
#include "ECS/Component/JewelScoreVisualComponent.h"
#include "JewelScoreWidget.generated.h"

UCLASS(Blueprintable, Abstract)
class JEWELECS_API UJewelScoreWidget : public UUserWidget, public IJewelScoreVisualComponent
{
	GENERATED_BODY()

public: // IJewelScoreVisualComponent
	UFUNCTION(BlueprintImplementableEvent)
		void SetScore(int32 Score) override;
};
