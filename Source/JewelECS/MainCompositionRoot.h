#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MainCompositionRoot.generated.h"

class AUnrealScheduler;

class UEntityObjectFactoryImpl;

class UJewelActorFactory;

class UPlayerInputManager;

UCLASS(Blueprintable)
class JEWELECS_API AMainCompositionRoot : public AActor
{
	GENERATED_BODY()

protected:
	void BeginPlay() final;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) final;

protected:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UJewelActorFactory> JewelFactoryClass;

private:
	UPROPERTY()
	AUnrealScheduler * scheduler;

	UPROPERTY()
	UPlayerInputManager * playerInput;

	UPROPERTY()
	UEntityObjectFactoryImpl * entityObjectFactory;

	UPROPERTY()
	UJewelActorFactory * jewelFactory;

	TSharedPtr<class ISystemRoot> systemRoot;
	TSharedPtr<class FComponentFactory> componentFactory;

private:
	void SetupSystems();
	void BuildEntitiesFromScene();
};
