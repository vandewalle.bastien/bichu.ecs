#include "EventObserver.h"

#include "Templates/Event/EventSubject.h"

FEventObserver::FEventObserver()
	: Subject(nullptr)
{
}

FEventObserver::~FEventObserver()
{
	StopObservingCurrentSubject();
}

void FEventObserver::SetSubject(FEventSubject * newSubject)
{
	Subject = newSubject;
}

void FEventObserver::StopObservingCurrentSubject()
{
	if (IsObserving())
	{
		Subject->UnsubscribeObserver(this);
		Subject = nullptr;
	}
}

void FEventObserver::AddHandleDelegate(const FName & name, const FDelegateHandle & handleDelegate)
{
	check(!HandleDelegates.Contains(name));

	HandleDelegates.Add(name, handleDelegate);
}

FDelegateHandle FEventObserver::PopHandleDelegate(const FName & name)
{
	check(HandleDelegates.Contains(name));

	FDelegateHandle handleDelegate = HandleDelegates[name];
	HandleDelegates.Remove(name);
	return handleDelegate;
}

bool FEventObserver::IsObserving() const
{
	return Subject != nullptr;
}

bool FEventObserver::IsObserving(const FEventSubject * subject) const
{
	return Subject != nullptr && Subject == subject;
}
