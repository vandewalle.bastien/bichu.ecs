#pragma once

#include "CoreMinimal.h"

class FEventSubject;

class JEWELECSCORE_API FEventObserver
{
	friend class FEventSubject;

public:
	FEventObserver();
	virtual ~FEventObserver();

public:
	bool IsObserving() const;
	bool IsObserving(const FEventSubject * subject) const;

public:
	void AddHandleDelegate(const FName & name, const FDelegateHandle & handleDelegate);
	FDelegateHandle PopHandleDelegate(const FName & name);

private:
	FEventSubject * Subject;
	TMap<FName, FDelegateHandle> HandleDelegates;

private:
	void SetSubject(FEventSubject * subject);
	void StopObservingCurrentSubject();
};
