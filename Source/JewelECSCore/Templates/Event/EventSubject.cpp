#include "EventSubject.h"

#include "Templates/Event/EventObserver.h"

void FEventSubject::UnsubscribeAllObservers()
{
	TArray<FEventObserver *> allObservers = Observers;

	for (FEventObserver * observer : allObservers)
	{
		UnsubscribeObserver(observer);
	}

	Observers.Empty();
}

void FEventSubject::SubscribeObserver(FEventObserver * observer)
{
	if (observer != nullptr)
	{
		if (observer->IsObserving())
		{
			LogAlreadyRegisteredMessage();

			observer->StopObservingCurrentSubject();
		}

		RegisterObserver(observer);
		OnBindAllDelegates(observer);
	}
}

void FEventSubject::RegisterObserver(FEventObserver * observer)
{
	check(observer != nullptr);
	check(!observer->IsObserving());
	check(!Observers.Contains(observer));

	observer->SetSubject(this);
	Observers.Add(observer);
}

void FEventSubject::UnsubscribeObserver(FEventObserver * observer)
{
	if (observer != nullptr)
	{
		if (observer->IsObserving(this))
		{
			OnUnbindAllDelegates(observer);
			UnregisterObserver(observer);
		}
		else
		{
			LogNotObservingMessage();
		}
	}
}

void FEventSubject::UnregisterObserver(FEventObserver * observer)
{
	check(observer != nullptr);
	check(observer->IsObserving(this));
	check(Observers.Contains(observer));

	observer->SetSubject(nullptr);
	Observers.Remove(observer);
}

#pragma region Log

void FEventSubject::LogAlreadyRegisteredMessage() const
{
#if !UE_BUILD_SHIPPING

	UE_LOG(LogTemp, Log, TEXT("This observer had already been previously registered."));

	FDebug::DumpStackTraceToLog();

#endif
}

void FEventSubject::LogNotObservingMessage() const
{
#if !UE_BUILD_SHIPPING

	UE_LOG(LogTemp, Log, TEXT("This observer is not observing this subject."));

	FDebug::DumpStackTraceToLog();

#endif
}

#pragma endregion Log
