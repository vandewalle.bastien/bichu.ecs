#pragma once

#include "CoreMinimal.h"

class FEventObserver;

class JEWELECSCORE_API FEventSubject
{
public:
	virtual ~FEventSubject() {}

public:
	void SubscribeObserver(FEventObserver * observer);
	void UnsubscribeObserver(FEventObserver * observer);

protected:
	virtual void OnBindAllDelegates(FEventObserver * observer) = 0;
	virtual void OnUnbindAllDelegates(FEventObserver * observer) = 0;

protected:
	// Call on destroy
	void UnsubscribeAllObservers();

private:
	TArray<FEventObserver *> Observers;

private:
	void RegisterObserver(FEventObserver * observer);
	void UnregisterObserver(FEventObserver * observer);

	FORCEINLINE void LogAlreadyRegisteredMessage() const;
	FORCEINLINE void LogNotObservingMessage() const;
};
