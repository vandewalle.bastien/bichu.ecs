using UnrealBuildTool;

public class JewelECSCore : ModuleRules
{
	public JewelECSCore(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bUseRTTI = true;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "BichuECS",
            "BichuCommon"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { });
    }
}
