// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Templates/Event/EventSubject.h"

class IMouseInputObserver;

DECLARE_MULTICAST_DELEGATE(FOnLeftClick)

class JEWELECSCORE_API FMouseInputDispatcher : public FEventSubject
{
public:
	FMouseInputDispatcher();
	~FMouseInputDispatcher();

public:
	void SubscribeObserver(IMouseInputObserver & observer);
	void UnsubscribeObserver(IMouseInputObserver & observer);

public:
	void BroadcastLeftClick() const;

protected: // FEventSubject
	void OnBindAllDelegates(FEventObserver * observer) final;
	void OnUnbindAllDelegates(FEventObserver * observer) final;

private:
	FOnLeftClick OnLeftClick;
};
