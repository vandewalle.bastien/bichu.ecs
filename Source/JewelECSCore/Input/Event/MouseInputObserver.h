#pragma once

#include "CoreMinimal.h"
#include "Templates/Event/EventObserver.h"

class JEWELECSCORE_API IMouseInputObserver : public FEventObserver
{
public:
	virtual ~IMouseInputObserver() {}

public:
	virtual void OnLeftClick() {}
};
