#include "MouseInputDispatcher.h"

#include "Input/Event/MouseInputObserver.h"

FMouseInputDispatcher::FMouseInputDispatcher()
{
}

FMouseInputDispatcher::~FMouseInputDispatcher()
{
	UnsubscribeAllObservers();
}

void FMouseInputDispatcher::SubscribeObserver(IMouseInputObserver & observer)
{
	FEventSubject::SubscribeObserver(&observer);
}

void FMouseInputDispatcher::UnsubscribeObserver(IMouseInputObserver & observer)
{
	FEventSubject::UnsubscribeObserver(&observer);
}

void FMouseInputDispatcher::OnBindAllDelegates(FEventObserver * observer)
{
	IMouseInputObserver * mouseInputObserver = static_cast<IMouseInputObserver *>(observer);

	if (mouseInputObserver == nullptr)
	{
		return;
	}

	mouseInputObserver->AddHandleDelegate(TEXT("OnLeftClick"), OnLeftClick.AddRaw(mouseInputObserver, &IMouseInputObserver::OnLeftClick));
}

void FMouseInputDispatcher::OnUnbindAllDelegates(FEventObserver * observer)
{
	IMouseInputObserver * mouseInputObserver = static_cast<IMouseInputObserver *>(observer);

	if (mouseInputObserver == nullptr)
	{
		return;
	}

	OnLeftClick.Remove(mouseInputObserver->PopHandleDelegate(TEXT("OnLeftClick")));
}

void FMouseInputDispatcher::BroadcastLeftClick() const
{
	if (OnLeftClick.IsBound())
	{
		OnLeftClick.Broadcast();
	}
}
