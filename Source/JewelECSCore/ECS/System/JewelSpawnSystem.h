#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "Bichu/ECS/ComponentNotication.h"
#include "ECS/Define/JewelDefine.h"

class IJewelFactory;
class IJewelGridComponent;

class JEWELECSCORE_API FJewelSpawnSystem final : public IEntityQuerierSystem, public IComponentNotication
{
public:
	FJewelSpawnSystem(IJewelFactory * jewelFactory);

public: // ISystem
	void Ready() override final;
	void Tick(float deltaSeconds) override final;

public: // IComponentNotication
	void NotifyComponentCreated(IComponent * Component) override final;
	void NotifyComponentDestroyed(IComponent * Component) override final {}

private:
	IJewelFactory * _jewelFactory;

private:
	void HandleGameStep(uint32 EntityId, EJewelGameStep Step);

	void SpawnJewels(
		bool MoveFromTop,
		bool AllowStrike) const;

	void SpawnGridJewels(
		IJewelGridComponent * JewelGrid,
		bool MoveFromTop = false,
		bool AllowStrike = false) const;

	void SpawnJewelAtPosition(
		IJewelGridComponent * JewelGrid, 
		int32 PosX,
		int32 PosY,
		EJewelColor Color,
		int32 SpawnHeight,
		bool MoveFromTop = true) const;

	void GoToMovingPhase() const;
};
