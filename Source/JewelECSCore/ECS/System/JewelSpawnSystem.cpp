#include "JewelSpawnSystem.h"

// Jewel
#include "ECS/Factory/JewelFactory.h"

// ECS
#include "ECS/Component/JewelGameStateComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelMovementComponent.h"

// Helper
#include "ECS/Helper/JewelGridHelper.h"
#include "ECS/Helper/JewelStrikeMarker.h"
#include "ECS/Helper/JewelSelectionHelper.h"

// Utility
#include "ECS/Utility/JewelGameStateUtility.h"
#include "ECS/Utility/JewelMovementUtility.h"
#include "ECS/Utility/JewelSpawnUtility.h"

FJewelSpawnSystem::FJewelSpawnSystem(IJewelFactory * jewelFactory)
	: _jewelFactory(jewelFactory)
{
}

void FJewelSpawnSystem::Ready()
{
	_jewelFactory->BuildJewelGrid();
}

void FJewelSpawnSystem::NotifyComponentCreated(IComponent * Component)
{
	if (Component->IsType<FJewelGameStateComponent>())
	{
		FJewelGameStateComponent * gameStateComp = static_cast<FJewelGameStateComponent *>(Component);

		gameStateComp->gameStep.NotifyOnValueSet(this, &FJewelSpawnSystem::HandleGameStep);

		SpawnJewels(false, false);
	}
}

void FJewelSpawnSystem::Tick(float deltaSeconds)
{
}

void FJewelSpawnSystem::HandleGameStep(uint32 EntityId, EJewelGameStep Step)
{
	if (Step != EJewelGameStep::JGS_Spawning)
	{
		return;
	}

	SpawnJewels(true, true);
}

void FJewelSpawnSystem::SpawnJewels(
	bool MoveFromTop,
	bool AllowStrike) const
{
	for (IJewelGridComponent * jewelGrid : GetEntityQueryRef().ComponentItr<IJewelGridComponent>())
	{
		SpawnGridJewels(jewelGrid, MoveFromTop, AllowStrike);
	}
}

void FJewelSpawnSystem::SpawnGridJewels(
	IJewelGridComponent * JewelGrid,
	bool MoveFromTop,
	bool AllowStrike) const
{
	const FJewelSelectionHelper selectionHelper(GetEntityQuery());
	const FJewelGridHelper gridHelper(GetEntityQuery());

	TArray<uint32> allJewelEntityIds;
	TArray<EJewelColor> allJewelColors;
	gridHelper.GetAllJewelColors(JewelGrid, allJewelEntityIds, allJewelColors);

	const TArray<EJewelColor> filledJewelColors = FJewelSpawnUtility::GetFilledJewelColors(
		allJewelColors,
		JewelGrid->GetWidth(),
		JewelGrid->GetHeight(),
		AllowStrike);

	for (int32 posX = 0; posX < JewelGrid->GetWidth(); posX++)
	{
		int32 spawnHeight = JewelGrid->GetHeight();

		for (int32 posY = 0; posY < JewelGrid->GetHeight(); posY++)
		{
			if (!selectionHelper.HasJewelAtLocation(JewelGrid, posX, posY))
			{
				const int32 jewelIndex = posY * JewelGrid->GetWidth() + posX;
				const EJewelColor jewelColor = filledJewelColors[jewelIndex];

				SpawnJewelAtPosition(JewelGrid, posX, posY, jewelColor, spawnHeight, MoveFromTop);

				spawnHeight++;
			}
		}
	}

	GoToMovingPhase();
}

void FJewelSpawnSystem::SpawnJewelAtPosition(
	IJewelGridComponent * JewelGrid,
	int32 PosX,
	int32 PosY,
	EJewelColor Color,
	int32 SpawnHeight,
	bool MoveFromTop) const
{
	const float gridSpacing = JewelGrid->GetSpacing();
	const uint32 jewelEntityId = _jewelFactory->BuildColorJewel(JewelGrid->GetEntityId(), Color, PosX, PosY, gridSpacing);

	if (MoveFromTop)
	{
		auto moveTuple = GetEntityQueryRef().GetTuple<FJewelInfoComponent, IJewelMovementComponent>(jewelEntityId);
		FJewelInfoComponent * jewelInfoComp = moveTuple.Get<0>();
		
		if (jewelInfoComp != nullptr)
		{
			FJewelMovementUtility::MoveJewel(jewelInfoComp, PosX, SpawnHeight, PosX, PosY, gridSpacing, EJewelMovement::JM_Fall);
		}
	}

	JewelGrid->JewelEntityIds.Add(jewelEntityId);
}

void FJewelSpawnSystem::GoToMovingPhase() const
{
	FJewelGameStateComponent * gameStateComp = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();
	FJewelGameStateUtility::GoToFallingPhase(gameStateComp, EJewelGameStep::JGS_None);
}
