#include "JewelGameModeSystem.h"

// Factory
#include "ECS/Factory/JewelFactory.h"

FJewelGameModeSystem::FJewelGameModeSystem(IJewelFactory * jewelFactory)
	: _jewelFactory(jewelFactory)
{
}

void FJewelGameModeSystem::Ready()
{
	_jewelFactory->BuildJewelGameMode();
}
