#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/System.h"

class IJewelFactory;

class JEWELECSCORE_API FJewelGameModeSystem final : public ISystem
{
public:
	FJewelGameModeSystem(IJewelFactory * jewelFactory);

public: // ISystem
	void Ready() override final;

private:
	IJewelFactory * _jewelFactory;
};
