#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "Input/Event/MouseInputObserver.h"

class JEWELECSCORE_API FSelectionSystem final : public IEntityQuerierSystem, public IMouseInputObserver
{
public: // ISystem
	void Ready() override final;

public: // IMouseInputObserver
	void OnLeftClick() override final;
};
