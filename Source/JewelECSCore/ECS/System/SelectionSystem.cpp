#include "SelectionSystem.h"

// ECS
#include "ECS/Component/SelectableComponent.h"

void FSelectionSystem::Ready()
{
}

void FSelectionSystem::OnLeftClick()
{
	for (ISelectableComponent * selectableComponent : GetEntityQueryRef().ComponentItr<ISelectableComponent>())
	{
		if (selectableComponent->IsOverlapped())
		{
			selectableComponent->Selection.SetValue(!selectableComponent->Selection.GetValue());
		}
	}
}
