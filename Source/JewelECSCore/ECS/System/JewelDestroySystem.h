#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "ECS/Define/JewelDefine.h"

class IJewelFactory;
class IJewelGridComponent;
class FJewelInfoComponent;

class JEWELECSCORE_API FJewelDestroySystem final : public IEntityQuerierSystem
{
public:
	FJewelDestroySystem(IJewelFactory * jewelFactory);

public: // ISystem
	void Tick(float deltaSeconds) override final;

private:
	IJewelFactory * _jewelFactory;

private:
	bool IsDestructionLocked() const;
	TArray<uint32> GetMarkedJewelEntityIds(IJewelGridComponent * jewelGridComp) const;

	void DestroyJewel(uint32 jewelEntityId) const;
	void GoToSpawnPhase() const;

	void OffsetJewels() const;
	void OffsetGridJewels(IJewelGridComponent * JewelGrid) const;
	void OffsetGridJewelsAtColumn(IJewelGridComponent * JewelGrid, int32 Column) const;
	FJewelInfoComponent * GetNextJewelAbove(IJewelGridComponent * JewelGrid, int32 Column, int32 Row) const;
};
