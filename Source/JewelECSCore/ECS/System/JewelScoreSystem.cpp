#include "JewelScoreSystem.h"

// ECS
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/JewelScoreInfoComponent.h"
#include "ECS/Component/JewelScoreVisualComponent.h"

// Factory
#include "ECS/Factory/JewelFactory.h"

FJewelScoreSystem::FJewelScoreSystem(IJewelFactory * jewelFactory)
{
	_jewelFactory = jewelFactory;
}

void FJewelScoreSystem::Ready()
{
	_jewelFactory->BuildJewelScore();
}

void FJewelScoreSystem::NotifyComponentCreated(IComponent * Component)
{
	if (Component->IsType<IJewelScoreVisualComponent>())
	{
		IJewelScoreVisualComponent * jewelScoreInfoComp = static_cast<IJewelScoreVisualComponent *>(Component);

		jewelScoreInfoComp->SetScore(0);
	}
}

void FJewelScoreSystem::NotifyComponentDestroyed(IComponent * Component)
{
	if (Component->IsType<FJewelInfoComponent>())
	{
		FJewelScoreInfoComponent * jewelScoreInfoComp = GetEntityQueryRef().GetSingletonComponent<FJewelScoreInfoComponent>();

		jewelScoreInfoComp->Score += 100;

		IJewelScoreVisualComponent * jewelScoreVisualComp = jewelScoreInfoComp->GetSibling<IJewelScoreVisualComponent>();

		jewelScoreVisualComp->SetScore(jewelScoreInfoComp->Score);
	}
}
