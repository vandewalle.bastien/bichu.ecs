#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "ECS/Define/JewelDefine.h"

class IJewelMovementComponent;
class FJewelInfoComponent;
class IJewelTransformComponent;

using FMoveTuple = TTuple<FJewelInfoComponent *, IJewelMovementComponent *, IJewelTransformComponent *>;

class JEWELECSCORE_API FJewelMoveSystem final : public IEntityQuerierSystem
{
public: // ISystem
	void Ready() override final;
	void Tick(float deltaSeconds) override final;

private:
	bool MoveAllJewels(float deltaSeconds);
	void MoveJewel(const FMoveTuple & MoveTuple, float deltaSeconds);
	bool IsFallingOrSwappingPhase() const;
	void GoToNextPhase() const;
};
