#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "Bichu/ECS/ComponentNotication.h"

class IJewelFactory;

class JEWELECSCORE_API FJewelScoreSystem final : public IEntityQuerierSystem, public IComponentNotication
{
public:
	FJewelScoreSystem(IJewelFactory * jewelFactory);

public: // IComponentNotication
	void NotifyComponentCreated(IComponent * Component) override final;
	void NotifyComponentDestroyed(IComponent * Component) override final;

public: // ISystem
	void Ready() override final;

private:
	IJewelFactory * _jewelFactory;
};
