#include "JewelMoveSystem.h"

// Component
#include "ECS/Component/JewelGameStateComponent.h"
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/JewelMovementComponent.h"
#include "ECS/Component/JewelTransformComponent.h"

void FJewelMoveSystem::Ready()
{
}

void FJewelMoveSystem::Tick(float deltaSeconds)
{
	if (IsFallingOrSwappingPhase())
	{
		if (!MoveAllJewels(deltaSeconds))
		{
			GoToNextPhase();
		}
	}
}

bool FJewelMoveSystem::IsFallingOrSwappingPhase() const
{
	FJewelGameStateComponent * gameState = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();
	check(gameState != nullptr);
	return gameState->gameStep.GetValue() == EJewelGameStep::JGS_Falling ||
		gameState->gameStep.GetValue() == EJewelGameStep::JGS_Swapping;
}

bool FJewelMoveSystem::MoveAllJewels(float deltaSeconds)
{
	bool areJewelMoving = false;

	for (const FMoveTuple & moveTuple : GetEntityQueryRef().TupleItr<FJewelInfoComponent, IJewelMovementComponent, IJewelTransformComponent>())
	{
		IJewelMovementComponent * moveComp = moveTuple.Get<1>();

		if (moveComp->IsMoving)
		{
			MoveJewel(moveTuple, deltaSeconds);

			areJewelMoving = true;
		}
	}

	return areJewelMoving;
}

void FJewelMoveSystem::MoveJewel(const FMoveTuple & MoveTuple, float deltaSeconds)
{
	IJewelMovementComponent * moveComp = MoveTuple.Get<1>();

	moveComp->MoveTime += deltaSeconds;

	const float lerp = FMath::SmoothStep(0.0f, moveComp->TransitionTime, moveComp->MoveTime);

	const FVector currentPosition = FMath::Lerp(moveComp->StartPos, moveComp->EndPos, lerp);

	IJewelTransformComponent * transformComp = MoveTuple.Get<2>();
	transformComp->SetPosition(currentPosition);

	if (lerp >= 1.0f)
	{
		moveComp->IsMoving = false;
	}
}

void FJewelMoveSystem::GoToNextPhase() const
{
	FJewelGameStateComponent * gameState = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();
	check(gameState != nullptr);
	gameState->gameStep.SetValue(gameState->postFallingGameStep);
}
