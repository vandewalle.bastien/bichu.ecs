#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "Bichu/ECS/ComponentNotication.h"

class IJewelGridComponent;
class FJewelInfoComponent;

class JEWELECSCORE_API FJewelSwapSystem final : public IEntityQuerierSystem, public IComponentNotication
{
public: // IComponentNotication
	void NotifyComponentCreated(IComponent * component) override final;

	void NotifyComponentDestroyed(IComponent * component) override final;

private:
	void OnJewelSelected(uint32 entityId, bool isSelected);
	bool CanJewelBeSelected(uint32 entityId) const;
	bool AttemptSwapJewel(uint32 entityId);
	bool IsSwappingLocked() const;
	void UnselectOtherJewelsOnGrid(uint32 entityId) const;
	bool WillSwapCauseStrike(IJewelGridComponent * grid, FJewelInfoComponent * lhs, FJewelInfoComponent * rhs) const;
	void GoToSwappingPhase() const;
};
