#include "JewelDestroySystem.h"

// ECS
#include "ECS/Component/JewelEffectComponent.h"
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/ColorJewelComponent.h"
#include "ECS/Component/JewelGameStateComponent.h"

// Factory
#include "ECS/Factory/JewelFactory.h"

// Helper
#include "ECS/Helper/JewelGridHelper.h"
#include "ECS/Helper/JewelSelectionHelper.h"
#include "ECS/Helper/JewelStrikeMarker.h"

// Utility
#include "ECS/Utility/JewelGameStateUtility.h"
#include "ECS/Utility/JewelMovementUtility.h"

FJewelDestroySystem::FJewelDestroySystem(IJewelFactory * jewelFactory)
	: _jewelFactory(jewelFactory)
{
}

void FJewelDestroySystem::Tick(float deltaSeconds)
{
	if (IsDestructionLocked())
	{
		return;
	}

	bool hasDestroyedJewels = false;

	for (IJewelGridComponent * jewelGridComp : GetEntityQueryRef().ComponentItr<IJewelGridComponent>())
	{
		const TArray<uint32> allMarkedJewelEntityIds = GetMarkedJewelEntityIds(jewelGridComp);

		for (const int32 markedJewelId : allMarkedJewelEntityIds)
		{
			jewelGridComp->JewelEntityIds.Remove(markedJewelId);

			DestroyJewel(markedJewelId);
		}

		if (allMarkedJewelEntityIds.Num() > 0)
		{
			hasDestroyedJewels = true;
		}
	}

	if (hasDestroyedJewels)
	{
		OffsetJewels();

		GoToSpawnPhase();
	}
}

bool FJewelDestroySystem::IsDestructionLocked() const
{
	FJewelGameStateComponent * gameStateComp = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();
	check(gameStateComp != nullptr);
	return gameStateComp->gameStep.GetValue() == EJewelGameStep::JGS_Falling ||
		gameStateComp->gameStep.GetValue() == EJewelGameStep::JGS_Swapping ||
		gameStateComp->gameStep.GetValue() == EJewelGameStep::JGS_Spawning;
}

TArray<uint32> FJewelDestroySystem::GetMarkedJewelEntityIds(IJewelGridComponent * jewelGridComp) const
{
	FJewelGridHelper gridHelper(GetEntityQuery());

	TArray<EJewelColor> allJewelColors;
	TArray<uint32> allJewelIds;
	gridHelper.GetAllJewelColors(jewelGridComp, allJewelIds, allJewelColors);

	const int32 width = jewelGridComp->GetWidth();
	const int32 height = jewelGridComp->GetHeight();

	const FJewelStrikeMarker rowMarker(allJewelColors, width, height);
	const TArray<int32> allMarkedJewelIndices = rowMarker.FindStrikeJewelIndices();

	TArray<uint32> markedJewelEntityIds;
	markedJewelEntityIds.Reserve(allMarkedJewelIndices.Num());

	for (int32 jewelIndex : allMarkedJewelIndices)
	{
		markedJewelEntityIds.Add(allJewelIds[jewelIndex]);
	}

	return markedJewelEntityIds;
}

void FJewelDestroySystem::DestroyJewel(uint32 jewelEntityId) const
{
	IJewelEffectComponent * effectComp = GetEntityQueryRef().GetComponent<IJewelEffectComponent>(jewelEntityId);
	
	if (effectComp != nullptr)
	{
		effectComp->PlayDestruction();
	}

	_jewelFactory->DestroyJewel(jewelEntityId);
}

#pragma region Jewel Offset

void FJewelDestroySystem::OffsetJewels() const
{
	for (IJewelGridComponent * gridComp : GetEntityQueryRef().ComponentItr<IJewelGridComponent>())
	{
		OffsetGridJewels(gridComp);
	}
}

void FJewelDestroySystem::OffsetGridJewels(IJewelGridComponent * JewelGrid) const
{
	for (int32 column = 0; column < JewelGrid->GetWidth(); column++)
	{
		OffsetGridJewelsAtColumn(JewelGrid, column);
	}
}

void FJewelDestroySystem::OffsetGridJewelsAtColumn(IJewelGridComponent * JewelGrid, int32 Column) const
{
	const FJewelSelectionHelper selectionHelper(GetEntityQuery());

	int32 highestRow = 0;

	for (int32 row = 0; row < JewelGrid->GetHeight(); row++)
	{
		if (!selectionHelper.HasJewelAtLocation(JewelGrid, Column, row))
		{
			const int32 rowAbove = FMath::Max(row, highestRow);

			FJewelInfoComponent * aboveJewel = GetNextJewelAbove(JewelGrid, Column, rowAbove);

			if (aboveJewel != nullptr)
			{
				highestRow = aboveJewel->PosY;

				FJewelMovementUtility::MoveJewel(aboveJewel, Column, row, JewelGrid->GetSpacing(), EJewelMovement::JM_Fall);
			}
			else
			{
				break;
			}
		}
	}
}

FJewelInfoComponent * FJewelDestroySystem::GetNextJewelAbove(IJewelGridComponent * JewelGrid, int32 Column, int32 Row) const
{
	const FJewelSelectionHelper selectionHelper(GetEntityQuery());

	for (int32 nextRow = Row + 1; nextRow < JewelGrid->GetHeight(); nextRow++)
	{
		FJewelInfoComponent * aboveJewel = selectionHelper.GetJewelAtLocation(JewelGrid, Column, nextRow);

		if (aboveJewel != nullptr)
		{
			return aboveJewel;
		}
	}

	return nullptr;
}

#pragma endregion Jewel Offset

void FJewelDestroySystem::GoToSpawnPhase() const
{
	FJewelGameStateComponent * gameStateComp = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();
	FJewelGameStateUtility::GoToPhase(gameStateComp, EJewelGameStep::JGS_Spawning);
}
