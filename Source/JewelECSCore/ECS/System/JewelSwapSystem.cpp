#include "JewelSwapSystem.h"

// ECS
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/JewelMovementComponent.h"
#include "ECS/Component/JewelGameStateComponent.h"
#include "ECS/Component/ColorJewelComponent.h"
#include "ECS/Component/SelectableComponent.h"

// Helper
#include "ECS/Helper/JewelGridHelper.h"
#include "ECS/Helper/JewelSelectionHelper.h"
#include "ECS/Helper/JewelStrikeMarker.h"

// Utility
#include "ECS/Utility/JewelGameStateUtility.h"
#include "ECS/Utility/JewelSwapUtility.h"

void FJewelSwapSystem::NotifyComponentCreated(IComponent * component)
{
	if (component->IsType<FJewelInfoComponent>())
	{
		ISelectableComponent * selectableComp = component->GetSibling<ISelectableComponent>();
		IColorJewelComponent * colorJewelComp = component->GetSibling<IColorJewelComponent>();

		if (selectableComp != nullptr && colorJewelComp != nullptr)
		{
			selectableComp->Selection.NotifyOnValueSet(this, &FJewelSwapSystem::OnJewelSelected);
			colorJewelComp->SetHighlight(false);
		}
	}
}

void FJewelSwapSystem::NotifyComponentDestroyed(IComponent * component)
{
}

void FJewelSwapSystem::OnJewelSelected(uint32 entityId, bool isSelected)
{
	IColorJewelComponent * colorJewelComp = GetEntityQueryRef().GetComponent<IColorJewelComponent>(entityId);
	check(colorJewelComp != nullptr)

	if (IsSwappingLocked() || !CanJewelBeSelected(entityId))
	{
		colorJewelComp->SetHighlight(false);
		return;
	}

	colorJewelComp->SetHighlight(isSelected);

	if (isSelected)
	{
		if (AttemptSwapJewel(entityId))
		{
			GoToSwappingPhase();
		}
		else
		{
			UnselectOtherJewelsOnGrid(entityId);
		}
	}
}

bool FJewelSwapSystem::IsSwappingLocked() const
{
	FJewelGameStateComponent * gameState = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();
	check(gameState != nullptr);
	return gameState->gameStep.GetValue() == EJewelGameStep::JGS_Swapping ||
		gameState->gameStep.GetValue() == EJewelGameStep::JGS_Destroying ||
		gameState->gameStep.GetValue() == EJewelGameStep::JGS_Spawning;
}

bool FJewelSwapSystem::CanJewelBeSelected(uint32 entityId) const
{
	IJewelMovementComponent * movementComp = GetEntityQueryRef().GetComponent<IJewelMovementComponent>(entityId);

	if (movementComp != nullptr)
	{
		return !movementComp->IsMoving;
	}

	return true;
}

bool FJewelSwapSystem::AttemptSwapJewel(uint32 entityId)
{
	FJewelSelectionHelper selectionHelper(GetEntityQuery());

	FJewelInfoComponent * jewelInfoComp = GetEntityQueryRef().GetComponent<FJewelInfoComponent>(entityId);
	check(jewelInfoComp != nullptr);

	const uint32 gridId = jewelInfoComp->GridId;
	IJewelGridComponent * jewelGridComp = GetEntityQueryRef().GetComponent<IJewelGridComponent>(gridId);
	const float gridSpacing = jewelGridComp->GetSpacing();
	check(jewelGridComp != nullptr);

	for (const TTuple<int32, int32> & neighbor : FJewelSwapUtility::GetNeighborLocations(jewelInfoComp))
	{
		const int32 jewelPosX = neighbor.Get<0>();
		const int32 jewelPosY = neighbor.Get<1>();

		FJewelInfoComponent * otherJewelInfoComp = selectionHelper.GetJewelAtLocation(jewelGridComp, jewelPosX, jewelPosY);

		if (FJewelSwapUtility::AreJewelBothSelected(jewelInfoComp, otherJewelInfoComp) &&
			WillSwapCauseStrike(jewelGridComp, jewelInfoComp, otherJewelInfoComp))
		{
			FJewelSwapUtility::SwapPosition(jewelInfoComp, otherJewelInfoComp, gridSpacing);
			FJewelSwapUtility::UnselectBothJewels(jewelInfoComp, otherJewelInfoComp);
			
			return true;
		}
	}

	return false;
}

bool FJewelSwapSystem::WillSwapCauseStrike(IJewelGridComponent * grid, FJewelInfoComponent * lhs, FJewelInfoComponent * rhs) const
{
	FJewelGridHelper gridHelper(GetEntityQuery());

	TArray<uint32> jewelIds;
	TArray<EJewelColor> jewelColors;
	gridHelper.GetAllJewelColors(grid, jewelIds, jewelColors);

	const int32 lhsIndex = jewelIds.IndexOfByKey(lhs->GetEntityId());
	const int32 rhsIndex = jewelIds.IndexOfByKey(rhs->GetEntityId());

	EJewelColor temp = jewelColors[lhsIndex];
	jewelColors[lhsIndex] = jewelColors[rhsIndex];
	jewelColors[rhsIndex] = temp;

	FJewelStrikeMarker rowMarker(jewelColors, grid->GetWidth(), grid->GetHeight());
	return rowMarker.FindStrikeJewelIndices().Num() > 0;
}

void FJewelSwapSystem::UnselectOtherJewelsOnGrid(uint32 entityId) const
{
	FJewelInfoComponent * jewelInfoComp = GetEntityQueryRef().GetComponent<FJewelInfoComponent>(entityId);
	check(jewelInfoComp != nullptr);

	FJewelSelectionHelper selectionHelper(GetEntityQuery());
	const uint32 gridId = jewelInfoComp->GridId;

	IJewelGridComponent * jewelGridComp = GetEntityQueryRef().GetComponent<IJewelGridComponent>(gridId);

	for (int32 y = 0; y < jewelGridComp->GetHeight(); y++)
	{
		for (int32 x = 0; x < jewelGridComp->GetWidth(); x++)
		{
			FJewelInfoComponent * otherJewelInfoComp = selectionHelper.GetJewelAtLocation(jewelGridComp, x, y);

			if (otherJewelInfoComp != nullptr && otherJewelInfoComp->GetEntityId() != entityId)
			{
				ISelectableComponent * otherJewelSelectableComp = otherJewelInfoComp->GetSibling<ISelectableComponent>();

				if (otherJewelSelectableComp != nullptr && otherJewelSelectableComp->Selection.GetValue())
				{
					otherJewelSelectableComp->Selection.SetValue(false);
				}
			}
		}
	}
}

void FJewelSwapSystem::GoToSwappingPhase() const
{
	FJewelGameStateComponent * gameState = GetEntityQueryRef().GetSingletonComponent<FJewelGameStateComponent>();

	if (gameState != nullptr)
	{
		gameState->postFallingGameStep = EJewelGameStep::JGS_Destroying;
		gameState->gameStep.SetValue(EJewelGameStep::JGS_Swapping);
	}
}
