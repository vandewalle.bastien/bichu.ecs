#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FComponentFactory;
class IEntityObjectFactory;

class IJewelFactory
{
public:
	virtual ~IJewelFactory() {}

public:
	virtual void Setup(IEntityObjectFactory * inEntityFactory, const TWeakPtr<FComponentFactory> & inComponentFactory) = 0;

public:
	virtual uint32 BuildColorJewel(uint32 inGridId, EJewelColor inColor, int32 inPosX, int32 inPosY, float inSpacing) const = 0;
	virtual void BuildJewelGrid() const = 0;
	virtual void BuildJewelGameMode() const = 0;
	virtual void BuildJewelScore() const = 0;

	virtual void DestroyJewel(uint32 EntityId) const = 0;
};
