#include "JewelGameStateUtility.h"

// ECS
#include "ECS/Component/JewelGameStateComponent.h"

void FJewelGameStateUtility::GoToPhase(FJewelGameStateComponent * gameState, EJewelGameStep gameStep)
{
	if (gameState != nullptr)
	{
		gameState->gameStep.SetValue(gameStep);
	}
}

void FJewelGameStateUtility::GoToFallingPhase(FJewelGameStateComponent * gameState, EJewelGameStep postFallingPhase)
{
	if (gameState != nullptr)
	{
		gameState->postFallingGameStep = postFallingPhase;
		gameState->gameStep.SetValue(EJewelGameStep::JGS_Falling);
	}
}
