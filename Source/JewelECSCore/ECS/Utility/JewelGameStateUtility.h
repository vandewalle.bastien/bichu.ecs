#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FJewelGameStateComponent;

class FJewelGameStateUtility
{
public:
	static void GoToPhase(FJewelGameStateComponent * gameState, EJewelGameStep gameStep);
	static void GoToFallingPhase(FJewelGameStateComponent * gameState, EJewelGameStep postFallingPhase);
};