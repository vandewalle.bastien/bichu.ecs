#pragma once

#include "CoreMinimal.h"

class FJewelInfoComponent;

class FJewelSwapUtility
{
public: // const
	static TArray<TTuple<int32, int32>> GetNeighborLocations(const FJewelInfoComponent * inJewelInfo);
	static bool AreJewelBothSelected(const FJewelInfoComponent * lhs, const FJewelInfoComponent * rhs);

public:
	static void UnselectBothJewels(FJewelInfoComponent * lhs, FJewelInfoComponent * rhs);
	static void SwapPosition(FJewelInfoComponent * lhs, FJewelInfoComponent * rhs, float GridSpacing);
};
