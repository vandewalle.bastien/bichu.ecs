#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FJewelInfoComponent;
class IJewelMovementComponent;

class FJewelMovementUtility
{
public:
	static void MoveJewel(
		FJewelInfoComponent * JewelInfo,
		int32 NewPosX,
		int32 NewPosY,
		float GridSpacing,
		EJewelMovement Movement);

	static void MoveJewel(
		FJewelInfoComponent * JewelInfo,
		int32 StartPosX,
		int32 StartPosY,
		int32 EndPosX,
		int32 EndPosY,
		float GridSpacing,
		EJewelMovement Movement);

private:
	static float GetSpeed(IJewelMovementComponent * MovementComp, EJewelMovement Movement);
};