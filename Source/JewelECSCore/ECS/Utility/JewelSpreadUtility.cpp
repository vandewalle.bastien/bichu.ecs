#include "JewelSpreadUtility.h"

int32 FJewelSpreadUtility::GetDirectionHorizontalOffset(EJewelSpreadDirection direction)
{
	switch (direction)
	{
	case EJewelSpreadDirection::JSD_Left:
		return -1;

	case EJewelSpreadDirection::JSD_Right:
		return 1;

	case EJewelSpreadDirection::JSD_Up:
	case EJewelSpreadDirection::JSD_Down:
		return 0;

	case EJewelSpreadDirection::JSD_Max:
		checkNoEntry();
		return 0;

	default:
		unimplemented();
		return 0;
	}
}

int32 FJewelSpreadUtility::GetDirectionVerticalOffset(EJewelSpreadDirection direction)
{
	switch (direction)
	{
	case EJewelSpreadDirection::JSD_Left:
	case EJewelSpreadDirection::JSD_Right:
		return 0;

	case EJewelSpreadDirection::JSD_Up:
		return 1;

	case EJewelSpreadDirection::JSD_Down:
		return -1;

	case EJewelSpreadDirection::JSD_Max:
		checkNoEntry();
		return 0;

	default:
		unimplemented();
		return 0;
	}
}
