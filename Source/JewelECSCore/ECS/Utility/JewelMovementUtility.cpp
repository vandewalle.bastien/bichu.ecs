#include "JewelMovementUtility.h"

// Component
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/JewelMovementComponent.h"

void FJewelMovementUtility::MoveJewel(
	FJewelInfoComponent * JewelInfo,
	int32 NewPosX,
	int32 NewPosY,
	float GridSpacing,
	EJewelMovement Movement)
{
	check(JewelInfo != nullptr);

	MoveJewel(JewelInfo,
		JewelInfo->PosX,
		JewelInfo->PosY,
		NewPosX,
		NewPosY,
		GridSpacing,
		Movement);
}

void FJewelMovementUtility::MoveJewel(
	FJewelInfoComponent * JewelInfo,
	int32 StartPosX,
	int32 StartPosY,
	int32 EndPosX,
	int32 EndPosY,
	float GridSpacing,
	EJewelMovement Movement)
{
	check(JewelInfo != nullptr);

	JewelInfo->PosX = EndPosX;
	JewelInfo->PosY = EndPosY;

	IJewelMovementComponent * moveComp = JewelInfo->GetSibling<IJewelMovementComponent>();

	if (moveComp != nullptr)
	{
		moveComp->StartPos = FVector(StartPosX * GridSpacing, StartPosY * GridSpacing, 0.0f);
		moveComp->EndPos = FVector(EndPosX * GridSpacing, EndPosY * GridSpacing, 0.0f);
		
		const float distance = FVector::Distance(moveComp->StartPos, moveComp->EndPos);
		const float speed = GetSpeed(moveComp, Movement);
		moveComp->TransitionTime = distance / speed;
		moveComp->MoveTime = 0.0f;
		moveComp->IsMoving = true;
	}
}

float FJewelMovementUtility::GetSpeed(IJewelMovementComponent * MovementComp, EJewelMovement Movement)
{
	check(MovementComp != nullptr);

	switch (Movement)
	{
	case EJewelMovement::JM_Fall:
		return MovementComp->GetFallSpeed();

	case EJewelMovement::JM_Swap:
		return MovementComp->GetSwapSpeed();

	default:
		unimplemented();
		return 0.0f;
	}
}
