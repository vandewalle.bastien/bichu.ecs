#include "JewelSpawnUtility.h"

// Helper
#include "ECS/Helper/JewelGridHelper.h"
#include "ECS/Helper/JewelStrikeMarker.h"

TArray<EJewelColor> FJewelSpawnUtility::GetFilledJewelColors(
	const TArray<EJewelColor> & JewelColors,
	int32 GridWidth,
	int32 GridHeight,
	bool AllowStrike)
{
	TArray<EJewelColor> allFilledJewelColors = JewelColors;

	for (int32 posY = 0; posY < GridHeight; posY++)
	{
		for (int32 posX = 0; posX < GridWidth; posX++)
		{
			const int32 jewelIndex = posY * GridWidth + posX;
			EJewelColor & jewelColor = allFilledJewelColors[jewelIndex];

			if (jewelColor == EJewelColor::JC_None)
			{
				if (AllowStrike)
				{
					jewelColor = GetRandomColor();
				}
				else
				{
					const TArray<EJewelColor> noStrikeColors = GetNoStrikeColors(allFilledJewelColors,
						jewelIndex,
						GridWidth,
						GridHeight);

					if (noStrikeColors.Num() == 0)
					{
						// Restart, chance it happens are pretty much 0
						checkNoEntry();
						return GetFilledJewelColors(JewelColors, GridWidth, GridHeight, AllowStrike);
					}

					jewelColor = GetRandomColor(noStrikeColors);
				}
			}
		}
	}

	return allFilledJewelColors;
}

EJewelColor FJewelSpawnUtility::GetRandomColor()
{
	const int32 randomColorIndex = FMath::RandRange(0, EJewelColor_MaxInt - 1);
	return static_cast<EJewelColor>(randomColorIndex);
}

TArray<EJewelColor> FJewelSpawnUtility::GetNoStrikeColors(
	TArray<EJewelColor> JewelColors,
	int32 JewelIndex,
	const int32 GridWidth,
	const int32 GridHeight)
{
	TArray<EJewelColor> allNoStrikeColor;

	for (const EJewelColor color : GetAllColors())
	{
		JewelColors[JewelIndex] = color;

		const FJewelStrikeMarker strikeMarker(JewelColors, GridWidth, GridHeight);
		const bool isStrike = strikeMarker.FindStrikeJewelIndices().Num() > 0;

		if (!isStrike)
		{
			allNoStrikeColor.Add(color);
		}
	}

	return allNoStrikeColor;
}

TArray<EJewelColor> FJewelSpawnUtility::GetAllColors()
{
	TArray<EJewelColor> allJewelColors;

	for (int32 jewelIndex = 0; jewelIndex < EJewelColor_MaxInt; jewelIndex++)
	{
		const EJewelColor jewelColor = static_cast<EJewelColor>(jewelIndex);
		allJewelColors.Add(jewelColor);
	}

	return allJewelColors;
}

EJewelColor FJewelSpawnUtility::GetRandomColor(const TArray<EJewelColor> & Colors)
{
	const int32 randomColorIndex = FMath::RandRange(0, Colors.Num() - 1);
	check(Colors.IsValidIndex(randomColorIndex));
	return Colors[randomColorIndex];
}
