#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FJewelSpreadUtility
{
public:
	static int32 GetDirectionHorizontalOffset(EJewelSpreadDirection direction);
	static int32 GetDirectionVerticalOffset(EJewelSpreadDirection direction);
};
