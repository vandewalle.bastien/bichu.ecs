#include "JewelSwapUtility.h"

// ECS
#include "ECS/Component/ColorJewelComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/SelectableComponent.h"

// Utility
#include "ECS/Utility/JewelMovementUtility.h"
#include "ECS/Utility/JewelSpreadUtility.h"

TArray<TTuple<int32, int32>> FJewelSwapUtility::GetNeighborLocations(const FJewelInfoComponent * inJewelInfo)
{
	const int32 posX = inJewelInfo->PosX;
	const int32 posY = inJewelInfo->PosY;

	TArray<TTuple<int32, int32>> neighborLocations = {
		TTuple<int32, int32>(posX + 1, posY),
		TTuple<int32, int32>(posX - 1, posY),
		TTuple<int32, int32>(posX, posY + 1),
		TTuple<int32, int32>(posX, posY - 1)
	};

	return neighborLocations;
}

bool FJewelSwapUtility::AreJewelBothSelected(const FJewelInfoComponent * lhs, const FJewelInfoComponent * rhs)
{
	if (lhs != nullptr && rhs != nullptr)
	{
		ISelectableComponent * lhsSelectableComp = lhs->GetSibling<ISelectableComponent>();
		ISelectableComponent * rhsSelectableComp = rhs->GetSibling<ISelectableComponent>();
		
		if (lhsSelectableComp != nullptr && rhsSelectableComp != nullptr)
		{
			return lhsSelectableComp->Selection.GetValue() &&
				rhsSelectableComp->Selection.GetValue();
		}
	}

	return false;
}

void FJewelSwapUtility::SwapPosition(FJewelInfoComponent * lhs, FJewelInfoComponent * rhs, float GridSpacing)
{
	const int32 lhsOldPosX = lhs->PosX;
	const int32 lhsOldPosY = lhs->PosY;
	
	const int32 rhsOldPosX = rhs->PosX;
	const int32 rhsOldPosY = rhs->PosY;

	FJewelMovementUtility::MoveJewel(lhs, rhsOldPosX, rhsOldPosY, GridSpacing, EJewelMovement::JM_Swap);
	FJewelMovementUtility::MoveJewel(rhs, lhsOldPosX, lhsOldPosY, GridSpacing, EJewelMovement::JM_Swap);
}

void FJewelSwapUtility::UnselectBothJewels(FJewelInfoComponent * lhs, FJewelInfoComponent * rhs)
{
	ISelectableComponent * lhsSelectableComp = lhs->GetSibling<ISelectableComponent>();
	check(lhsSelectableComp != nullptr);

	lhsSelectableComp->Selection.SetValue(false);

	ISelectableComponent * rhsSelectableComp = rhs->GetSibling<ISelectableComponent>();
	check(rhsSelectableComp != nullptr);

	rhsSelectableComp->Selection.SetValue(false);
}
