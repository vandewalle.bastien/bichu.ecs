#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FJewelSpawnUtility
{
public:
	static TArray<EJewelColor> GetFilledJewelColors(
		const TArray<EJewelColor> & JewelColors,
		int32 GridWidth,
		int32 GridHeight, 
		bool AllowStrike = true);

private:
	static EJewelColor GetRandomColor();
	static TArray<EJewelColor> GetAllColors();
	static EJewelColor GetRandomColor(const TArray<EJewelColor> & Colors);

private:
	static TArray<EJewelColor> GetNoStrikeColors(
		TArray<EJewelColor> JewelColors,
		int32 JewelIndex,
		const int32 GridWidth,
		const int32 GridHeight);
};
