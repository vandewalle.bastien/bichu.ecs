#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FEntityQuery;
class IJewelGridComponent;

class FJewelGridHelper
{
public:
	FJewelGridHelper(const TWeakPtr<FEntityQuery> & inEntityQuery);

public:
	void GetAllJewelColors(IJewelGridComponent * JewelGrid, TArray<uint32> & outEntityIds, TArray<EJewelColor> & outColors) const;

private:
	TWeakPtr<FEntityQuery> entityQuery;
};
