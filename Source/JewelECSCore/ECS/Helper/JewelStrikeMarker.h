#pragma once

#include "CoreMinimal.h"
#include "ECS/Define/JewelDefine.h"

class FJewelStrikeMarker
{
public:
	FJewelStrikeMarker(const TArray<EJewelColor> & allJewels, int32 width, int32 height);
	
public:
	TArray<int32> FindStrikeJewelIndices() const;

private:
	const TArray<EJewelColor> _jewels;
	const int32 _width;
	const int32 _height;
	mutable TSet<int32> _markedJewelIndices;

private:
	bool HasBeenMarked(int32 x, int32 y) const;
	EJewelColor GetJewelColor(int32 x, int32 y) const;
	int32 CalculateJewelIndex(int32 x, int32 y) const;
	void MarkHorizontalSpread(int32 x, int32 y, EJewelColor color) const;
	void MarkVerticalSpread(int32 x, int32 y, EJewelColor color) const;
	int32 EvaluateJewelSpread(int32 x, int32 y, int32 length, EJewelColor color, EJewelSpreadDirection direction) const;
	bool IsInRange(int32 x, int32 y) const;
};
