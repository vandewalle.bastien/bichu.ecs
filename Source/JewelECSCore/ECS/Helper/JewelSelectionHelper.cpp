#include "JewelSelectionHelper.h"

// ECS
#include "Bichu/ECS/EntityQuery.h"

// Component
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelInfoComponent.h"
#include "ECS/Component/ColorJewelComponent.h"

FJewelSelectionHelper::FJewelSelectionHelper(const TWeakPtr<FEntityQuery> & inEntityQuery)
	: entityQuery(inEntityQuery)
{
}

FJewelInfoComponent * FJewelSelectionHelper::GetJewelAtLocation(IJewelGridComponent * inJewelMap, int32 inPosX, int32 inPosY) const
{
	for (int32 jewelIndex = 0; jewelIndex < inJewelMap->JewelEntityIds.Num(); jewelIndex++)
	{
		const uint32 entityId = inJewelMap->JewelEntityIds[jewelIndex];

		FJewelInfoComponent * jewelInfoComp = entityQuery.Pin()->GetComponent<FJewelInfoComponent>(entityId);

		if (jewelInfoComp->PosX == inPosX && jewelInfoComp->PosY == inPosY)
		{
			return jewelInfoComp;
		}
	}

	return nullptr;
}

bool FJewelSelectionHelper::HasJewelAtLocation(IJewelGridComponent * inJewelMap, int32 inPosX, int32 inPosY) const
{
	return GetJewelAtLocation(inJewelMap, inPosX, inPosY) != nullptr;
}
