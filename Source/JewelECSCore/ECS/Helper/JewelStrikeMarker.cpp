#include "JewelStrikeMarker.h"

#include "ECS/Utility/JewelSpreadUtility.h"

FJewelStrikeMarker::FJewelStrikeMarker(const TArray<EJewelColor> & allJewels, int32 width, int32 height)
	: _jewels(allJewels)
	, _width(width)
	, _height(height)
{
}

TArray<int32> FJewelStrikeMarker::FindStrikeJewelIndices() const
{
	_markedJewelIndices.Empty();

	for (int32 y = 0; y < _height; y++)
	{
		for (int32 x = 0; x < _width; x++)
		{
			if (HasBeenMarked(x, y))
			{
				continue;
			}

			const EJewelColor color = GetJewelColor(x, y);

			MarkHorizontalSpread(x, y, color);
			MarkVerticalSpread(x, y, color);
		}
	}

	return _markedJewelIndices.Array();
}

bool FJewelStrikeMarker::HasBeenMarked(int32 x, int32 y) const
{
	const int32 jewelIndex = CalculateJewelIndex(x, y);
	return _markedJewelIndices.Contains(jewelIndex);
}

EJewelColor FJewelStrikeMarker::GetJewelColor(int32 x, int32 y) const
{
	const int32 jewelIndex = CalculateJewelIndex(x, y);
	return _jewels[jewelIndex];
}

void FJewelStrikeMarker::MarkHorizontalSpread(int32 x, int32 y, EJewelColor color) const
{
	int32 rightSpread = EvaluateJewelSpread(x + 1, y, 0, color, EJewelSpreadDirection::JSD_Right);
	int32 leftSpread = EvaluateJewelSpread(x - 1, y, 0, color, EJewelSpreadDirection::JSD_Left);

	if (rightSpread + leftSpread + 1 >= JewelDef::JEWEL_STRIKE_MIN)
	{
		for (int32 markedJewelX = x - leftSpread; markedJewelX <= x + rightSpread; markedJewelX++)
		{
			_markedJewelIndices.Add(CalculateJewelIndex(markedJewelX, y));
		}
	}
}

void FJewelStrikeMarker::MarkVerticalSpread(int32 x, int32 y, EJewelColor color) const
{
	int32 upSpread = EvaluateJewelSpread(x, y + 1, 0, color, EJewelSpreadDirection::JSD_Up);
	int32 downSpread = EvaluateJewelSpread(x, y - 1, 0, color, EJewelSpreadDirection::JSD_Down);

	if (upSpread + downSpread + 1 >= JewelDef::JEWEL_STRIKE_MIN)
	{
		for (int32 markedJewelY = y - downSpread; markedJewelY <= y + upSpread; markedJewelY++)
		{
			_markedJewelIndices.Add(CalculateJewelIndex(x, markedJewelY));
		}
	}
}

int32 FJewelStrikeMarker::EvaluateJewelSpread(int32 x, int32 y, int32 length, EJewelColor color, EJewelSpreadDirection direction) const
{
	if (!IsInRange(x, y) || color == EJewelColor::JC_None)
	{
		return length;
	}

	if (GetJewelColor(x, y) != color)
	{
		return length;
	}

	int32 newPosX = x + FJewelSpreadUtility::GetDirectionHorizontalOffset(direction);
	int32 newPosY = y + FJewelSpreadUtility::GetDirectionVerticalOffset(direction);

	return EvaluateJewelSpread(newPosX, newPosY, length + 1, color, direction);
}

int32 FJewelStrikeMarker::CalculateJewelIndex(int32 x, int32 y) const
{
	check(IsInRange(x, y));
	return y * _width + x;
}

bool FJewelStrikeMarker::IsInRange(int32 x, int32 y) const
{
	return x >= 0 && x < _width &&
		y >= 0 && y < _height;
}
