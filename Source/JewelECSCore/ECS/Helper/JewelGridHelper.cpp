#include "JewelGridHelper.h"

// Component
#include "ECS/Component/ColorJewelComponent.h"
#include "ECS/Component/JewelGridComponent.h"
#include "ECS/Component/JewelInfoComponent.h"

// Helper
#include "ECS/Helper/JewelSelectionHelper.h"

FJewelGridHelper::FJewelGridHelper(const TWeakPtr<FEntityQuery> & inEntityQuery)
	: entityQuery(inEntityQuery)
{
}

void FJewelGridHelper::GetAllJewelColors(
	IJewelGridComponent * JewelGrid,
	TArray<uint32> & outEntityIds,
	TArray<EJewelColor> & outColors) const
{
	const int32 width = JewelGrid->GetWidth();
	const int32 height = JewelGrid->GetHeight();

	const int32 size = width * height;

	const FJewelSelectionHelper selectionHelper(entityQuery);

	for (int32 y = 0; y < height; y++)
	{
		for (int32 x = 0; x < width; x++)
		{
			FJewelInfoComponent * jewelInfoComp = selectionHelper.GetJewelAtLocation(JewelGrid, x, y);

			EJewelColor color = EJewelColor::JC_None;
			uint32 entityId = 0;

			if (jewelInfoComp)
			{
				entityId = jewelInfoComp->GetEntityId();

				IColorJewelComponent * colorJewelComp = jewelInfoComp->GetSibling<IColorJewelComponent>();

				if (colorJewelComp != nullptr)
				{
					color = colorJewelComp->GetColor();
				}
			}

			outColors.Add(color);
			outEntityIds.Add(entityId);
		}
	}
}
