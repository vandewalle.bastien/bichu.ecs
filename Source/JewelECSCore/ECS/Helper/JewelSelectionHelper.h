#pragma once

#include "CoreMinimal.h"

class FEntityQuery;

class FJewelInfoComponent;
class IJewelGridComponent;

class FJewelSelectionHelper
{
public:
	FJewelSelectionHelper(const TWeakPtr<FEntityQuery> & inEntityQuery);

public:
	FJewelInfoComponent * GetJewelAtLocation(IJewelGridComponent * inJewelMap, int32 inPosX, int32 inPosY) const;
	bool HasJewelAtLocation(IJewelGridComponent * inJewelMap, int32 inPosX, int32 inPosY) const;

private:
	TWeakPtr<FEntityQuery> entityQuery;
};
