#pragma once

#include "CoreMinimal.h"

namespace JewelDef
{
	static const int32 JEWEL_STRIKE_MIN = 3;
}

enum class EJewelColor : uint8
{
	JC_Red,
	JC_Blue,
	JC_Green,
	JC_Yellow,

	JC_Max,

	JC_None,
};
#define EJewelColor_MaxInt    static_cast<int32>(EJewelColor::JC_Max)

enum class EJewelSpreadDirection : uint8
{
	JSD_Up,
	JSD_Right,
	JSD_Down,
	JSD_Left,

	JSD_Max,
};
#define EJewelSpreadDirection_MaxInt    static_cast<int32>(EJewelSpreadDirection::JSD_Max) const

enum class EJewelGameStep : uint8
{
	JGS_None,
	JGS_Falling,
	JGS_Swapping,
	JGS_Spawning,
	JGS_Destroying,
};

enum class EJewelMovement : uint8
{
	JM_Swap,
	JM_Fall,
};
