#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"
#include "ECS/Define/JewelDefine.h"

class JEWELECSCORE_API FJewelInfoComponent : public IComponent
{
public:
	FJewelInfoComponent();
	FJewelInfoComponent(uint32 inGridId, int32 inPosX, int32 inPosY);

public:
	uint32 GridId;
	int32 PosX;
	int32 PosY;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_INFO_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_INFO_COMPONENT_TYPE;
};
