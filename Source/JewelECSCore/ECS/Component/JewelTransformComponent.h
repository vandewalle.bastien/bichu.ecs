#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class JEWELECSCORE_API IJewelTransformComponent : public IComponent
{
public:
	virtual ~IJewelTransformComponent() {}

public: // virtual
	virtual void SetPosition(const FVector & Position) = 0;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_POSITION_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_POSITION_COMPONENT_TYPE;
};
