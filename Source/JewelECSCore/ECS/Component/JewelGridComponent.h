#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class UWidget;

class JEWELECSCORE_API IJewelGridComponent : public IComponent
{
public:
	virtual ~IJewelGridComponent() {}

public:
	TArray<uint32> JewelEntityIds;

public: // virtual
	virtual int32 GetWidth() const = 0;
	virtual int32 GetHeight() const = 0;

	virtual float GetSpacing() const = 0;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_GRID_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_GRID_COMPONENT_TYPE;
};
