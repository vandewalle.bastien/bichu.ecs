#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class JEWELECSCORE_API FJewelScoreInfoComponent : public IComponent
{
public:
	FJewelScoreInfoComponent();

public:
	int32 Score;

public: // IComponent
	const type_info & GetType() const final;

private:
	static const type_info & JEWEL_SCORE_INFO_COMPONENT_TYPE;
};
