#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

#include "Bichu/Common/Dispatcher/DispatchOnSet.h"

#include "ECS/Define/JewelDefine.h"

class JEWELECSCORE_API FJewelGameStateComponent : public IComponent
{
public:
	TDispatchOnSet<EJewelGameStep> gameStep;
	EJewelGameStep postFallingGameStep;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_GAME_STATE_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_GAME_STATE_COMPONENT_TYPE;
};
