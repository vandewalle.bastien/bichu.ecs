#include "JewelInfoComponent.h"

const type_info & FJewelInfoComponent::JEWEL_INFO_COMPONENT_TYPE = typeid(FJewelInfoComponent);

FJewelInfoComponent::FJewelInfoComponent()
	: GridId(0)
	, PosX(0)
	, PosY(0)
{
}

FJewelInfoComponent::FJewelInfoComponent(uint32 inGridId, int32 inPosX, int32 inPosY)
	: GridId(inGridId)
	, PosX(inPosX)
	, PosY(inPosY)
{
}
