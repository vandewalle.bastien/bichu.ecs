#include "JewelScoreInfoComponent.h"

const type_info & FJewelScoreInfoComponent::JEWEL_SCORE_INFO_COMPONENT_TYPE = typeid(FJewelScoreInfoComponent);

FJewelScoreInfoComponent::FJewelScoreInfoComponent()
	: Score(0)
{
}

const type_info & FJewelScoreInfoComponent::GetType() const
{
	return JEWEL_SCORE_INFO_COMPONENT_TYPE;
}
