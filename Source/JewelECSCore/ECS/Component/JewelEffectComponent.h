#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class JEWELECSCORE_API IJewelEffectComponent : public IComponent
{
public:
	virtual ~IJewelEffectComponent() {}

public: // virtual
	virtual void PlayDestruction() const = 0;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_EFFECT_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_EFFECT_COMPONENT_TYPE;
};
