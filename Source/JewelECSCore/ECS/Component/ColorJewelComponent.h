#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"
#include "ECS/Define/JewelDefine.h"

class JEWELECSCORE_API IColorJewelComponent : public IComponent
{
public:
	virtual ~IColorJewelComponent() {}

public: // virtual
	virtual void SetColor(const EJewelColor Color) = 0;
	virtual EJewelColor GetColor() const = 0;

	virtual void SetHighlight(bool isHighlighted) = 0;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_VISUAL_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_VISUAL_COMPONENT_TYPE;
};
