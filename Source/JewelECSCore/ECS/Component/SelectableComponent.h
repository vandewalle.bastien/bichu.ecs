#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"
#include "Bichu/Common/Dispatcher/DispatchOnSet.h"

class JEWELECSCORE_API ISelectableComponent : public IComponent
{
public:
	virtual ~ISelectableComponent() {}

public: // virtual
	virtual bool IsOverlapped() const = 0;

public:
	TDispatchOnSet<bool> Selection;

public: // IComponent
	const type_info & GetType() const final { return SELECTABLE_COMPONENT_TYPE; }

private:
	static const type_info & SELECTABLE_COMPONENT_TYPE;
};
