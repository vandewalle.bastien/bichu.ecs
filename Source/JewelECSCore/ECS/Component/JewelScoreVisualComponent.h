#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class JEWELECSCORE_API IJewelScoreVisualComponent : public IComponent
{
public:
	virtual ~IJewelScoreVisualComponent() {}

public: // virtual
	virtual void SetScore(int32 score) = 0;

public: // IComponent
	const type_info & GetType() const final;

private:
	static const type_info & JEWEL_SCORE_VISUAL_COMPONENT_TYPE;
};
