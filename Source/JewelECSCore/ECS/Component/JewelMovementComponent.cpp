#include "JewelMovementComponent.h"

const type_info & IJewelMovementComponent::JEWEL_MOVEMENT_COMPONENT_TYPE = typeid(IJewelMovementComponent);

IJewelMovementComponent::IJewelMovementComponent()
	: IsMoving(false)
	, MoveTime(0.0f)
	, TransitionTime(0.0f)
	, StartPos()
	, EndPos()
{
}
