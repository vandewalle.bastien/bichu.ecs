#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"
#include "ECS/Define/JewelDefine.h"

class JEWELECSCORE_API IJewelMovementComponent : public IComponent
{
public:
	IJewelMovementComponent();
	virtual ~IJewelMovementComponent() {}

public:
	bool IsMoving;
	float MoveTime;
	float TransitionTime;
	FVector StartPos;
	FVector EndPos;

public: // virtual
	virtual float GetFallSpeed() const = 0;
	virtual float GetSwapSpeed() const = 0;

public: // IComponent
	const type_info & GetType() const final { return JEWEL_MOVEMENT_COMPONENT_TYPE; }

private:
	static const type_info & JEWEL_MOVEMENT_COMPONENT_TYPE;
};
