#include "JewelScoreVisualComponent.h"

const type_info & IJewelScoreVisualComponent::JEWEL_SCORE_VISUAL_COMPONENT_TYPE = typeid(IJewelScoreVisualComponent);

const type_info & IJewelScoreVisualComponent::GetType() const
{
	return JEWEL_SCORE_VISUAL_COMPONENT_TYPE;
}
